import React from 'react';
import { StyleSheet, Text, View, Button, Alert, TextInput, Dimensions,Image,ImageBackground } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
import { Icon } from 'react-native-elements';
import { ImagePicker, DocumentPicker } from 'expo';
import bgImage from './Img/wallpaper.png';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      isImageSelected: false,

    };
    this.onSend = this.onSend.bind(this);
    this.sendAttach = this.sendAttach.bind(this);
  }

  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
            attatch:'https://facebook.github.io/react/img/logo_og.png',

          }
        },
      ],
    });
  }

  onSend(messages = []) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages ),
      };
    });
  }

  sendAttach() {
    let formdata = new FormData();

    if (this.state.isImageSelected)
      formdata.append("upload", {uri: this.state.messages, name: 'image.jpg', type: 'multipart/form-data'})
    console.log('Hi Dost');

  }


  _takeCamera = async() => {
    let result = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    console.log(result);

    if (!result.cancelled) {
      console.log("cam", result.uri)
      this.setState({messages: result.uri});
    }
  };

  _pickImage = async() => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    console.log(result);

    if (!result.cancelled) {
      this.setState({messages: result.uri});
    }
  };


  uploadDocument = () => {
    console.log('data uploading');
    Alert.alert(
        'Add Document',
        'Select the option to load the document',
        [
          {text: 'CAMERA', onPress: this._takeCamera},
          {text: 'GALLERY', onPress: this._pickImage},

        ],
        {cancelable: true}
    )
  };



  render() {
    return (


     /*<GiftedChat
            messages={this.state.messages}
             onSend={this.onSend}
             user={{
             _id: 1,
             }}
         />*/



        /*<View style={styles.camera}>
               <Button
                    title="Camera"
                    backgroundColor="#01a0e2"
                    buttonStyle={{margin:10}}
                    onPress={ this.uploadDocument} />

          {this.state.image &&
          <Image source={{ uri: this.state.image }} style={{ width: 200, height: 200,alignSelf:'center' }} />}
        </View>*/



<View style={styles.picture}>
            <ImageBackground style={styles.picture} source={bgImage}>
              <GiftedChat
                  styles={{margin:100}}
                  messages={this.state.messages}
                  onSend={this.onSend}
                  user={{
             _id: 1,
             }}
              />


            </ImageBackground>

      <View style={{flexDirection:'row', backgroundColor:'#00cccc'}}>
            <Icon
                raised
                name='camera'
                type='font-awesome'
                color='#888888'
                onPress={ this._takeCamera }/>


            <Icon
                raised
                name='file-image-o'
                type='font-awesome'
                color='#ff8080'
                onPress={this._pickImage} />

            <Icon
                raised
                name='file'
                type='font-awesome'
                color='#bcd639'
                onPress={this._pickImage} />





      </View>

</View>


                    );

  }

}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    margin: 4,
    height: 40,
    borderColor: '#585858',
    borderWidth: 1,
    paddingLeft:8,

  },
  camera:{
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  picture:{
    flex: 1,

  }
});
